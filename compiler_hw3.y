/*	Definition section */

%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define CSIZE 40
extern int yylineno;
extern int yylex();
extern FILE *yyin;
extern char a[4096];

FILE *to_j;
char outputStr[CSIZE][4096];
char nowType, preType, st = 'B';
char typestack[20];
char preStack;
int maxS = 0, tyi = 0, na = 0, hs = 0, hf = 0, hi = 0, af = 0, stacksize = 0, ifer = 0, lab = 0, may = 0;
int mayAdd[20];

char mID[20];
char lockedID[20];
char mType[8];
int I_data;
float F_data;
char mStr[87];
int printErrflag = 0;
int initflag = 0;
int isflt = 0;
int declaredtwice = 0;
int ScopeDepth = 0;
int MaxScopeDepth = 0;
int l = 0;
int r = 0;

int INDEX = 0;
int EXITID = 0;

typedef struct symbol_table
{
	int ScopeDepth;
	int scopeindex;

	char mID[20];
	char mType[8];
	int I_data;
	float F_data;
	struct symbol_table *next;

	int index;

}symbol_table;

typedef struct scope	// include symbol_table
{
	struct scope *child;
	struct scope *mother;

	symbol_table *inScope_head;
	symbol_table *inScope_list;

	int scopeindex;
	int IFIF;
}scope;

int scopeindex = 0;		// 每個scope有自己的標籤
scope *scopelist[256];	// 在 dump 時使用, print所有的variable

/* Symbol table function - you can add new function if need. */
symbol_table *lookup_symbol(char const *); // 搜尋 symbol_table, return NULL 或一個 symbol_table
void create_symbol();			// 初始化 - print "Create symbol table", 之後都交給 insert_symbol
void insert_symbol();			// 對每個Scope的symbol_table做賦值
void dump_symbol();				// 印出所有的變數資訊
void IAlwaysInit();				// 避免 Core dump 在 main() 直接做初始化
void scopefunc(char);			// 處理 '{' 和 '}'
float Func_Assign(char, float);	// 處理 Assign Op

void IncDecFunc(char);
void checki2f(char);
void scopetype(char);
void pfunc(float, int);
void pfuncS(int);

// 必要的函式 
void yyerror(char const *s) { ifer++; fprintf(stderr, ANSI_COLOR_RED	"<Error> %s (line %d)\n"	ANSI_COLOR_RESET, s, yylineno); }

symbol_table *gbTmp;	// 用於 symboltable 的暫存
scope *Scope, *MasterScope; //Scope 會一直變, MasterScope 作為所有 Scope 的祖先

%}

/* Using union to define nonterminal and token type */
%union {
	int i_val;
	double f_val;
	char* string;
}

/* OTHERS PLEASE RETURN *YYTEXT */

/* Token without return NEED RETURN ! BUT NO TYPE*/
%token PRINT PRINTLN 
%token IF ELSE FOR ELIF
%token VAR NEWLINE
%token INT FLOAT VOID
%token INCREMENT DECREMENT 
%token Add_Assign Sub_Assign Mul_Assign Div_Assign Mod_Assign
%token GRE LSE EQU NEQ
%token AND OR
%token Other

/* Token with return, which need to sepcify type */
%token <i_val> I_CONST
%token <f_val> F_CONST
%token <string> STRING
%token <string> ID

/* Nonterminal with return, which need to sepcify type */
%type <f_val> CALC
%type <f_val> hiCALC
%type <f_val> RELATIVE
%type <f_val> expr
%type <f_val> IncDecStmt
%type <f_val> STORE_ID
%type <f_val> STORE_INT
%type <f_val> STORE_FLT
%type <f_val> lockedID

/* Yacc will start at this nonterminal */
%start program

/* Grammar section */
%%

program 
	: stmt program 
	|
;

stmt
	: dcl 		
	| expr
	| print_func
	| IfStmt
	| ForStmt
	| trap
	| comp
;

ForStmt : FOR '(' expr ')' stmt { printf("For Stmt"); st = 'F';}
;

IfStmt 
	: IF expr { Scope -> mother -> IFIF ++; st = 'I';} comp
	| IF '(' expr ')' { Scope -> mother -> IFIF ++; st = 'I';} comp
	| ELIF expr { if(Scope -> mother -> IFIF <= 0){ ifer++; yyerror("<ELSE IF> used without <IF>");} st = 'J';} comp
	| ELIF '(' expr ')' { if(Scope -> mother -> IFIF <= 0){ ifer++; yyerror("<ELSE IF> used without <IF>"); } st = 'J';} comp
	| ELSE { Scope -> mother -> IFIF--; if(Scope -> mother -> IFIF < 0){ ifer++; yyerror("<ELSE> used without <IF> or <ELSE> used twice "); } st = 'K';} comp
;

comp
	: '{' {scopefunc('{'); lab++;} program '}' {scopefunc('}'); lab++; scopetype(st); }
;

dcl	: VAR lockedID type '=' CALC	{ I_data = (int)$5; F_data = $5; create_symbol(); }
	| VAR lockedID type		{ I_data = 0; F_data = 0; na = 1; create_symbol(); }
;

expr	
	:	CALC						{ $$ = $1;}
	|	RELATIVE					{ $$ = $1;}
	|	lockedID '=' CALC 			{ $$ = Func_Assign('=', $3); }
	|	lockedID Add_Assign CALC	{ $$ = Func_Assign('+', $3); }
	|	lockedID Sub_Assign CALC	{ $$ = Func_Assign('-', $3); }
	|	lockedID Mul_Assign CALC	{ $$ = Func_Assign('*', $3); }
	|	lockedID Div_Assign CALC	{ $$ = Func_Assign('/', $3); }
	|	lockedID Mod_Assign CALC	{ $$ = Func_Assign('%', $3); }
;

print_func 
	: PRINT '(' expr ')' 	{ if(printErrflag == 1) {  printErrflag = 0; } else pfunc($3, 0); }
	| PRINTLN '(' expr ')' 	{ if(printErrflag == 1) {  printErrflag = 0; } else pfunc($3, 1); }
	| PRINT '(' STORE_STR ')' 	{ pfuncS(0); }
	| PRINTLN '(' STORE_STR ')'	{ pfuncS(1); }
;

type
	: INT 		{ strcpy(mType, "int"); }
	| FLOAT 	{ strcpy(mType, "float32"); }
	| VOID 		{ strcpy(mType, "void"); }
;

IncDecStmt
	:	INCREMENT IncDecStmt { char reg[200]; if(nowType == 'A') sprintf(reg, "\tldc %d\n\tiadd\n\tistore %d\n", 1,gbTmp->index-1); else sprintf(reg, "\tldc %f\n\tfadd\n\tfstore %d\n", 1.0,gbTmp->index-1); strcat(outputStr[lab], reg);}
	|	DECREMENT IncDecStmt { char reg[200]; if(nowType == 'A') sprintf(reg, "\tldc %d\n\tisub\n\tistore %d\n", 1,gbTmp->index-1); else sprintf(reg, "\tldc %f\n\tfsub\n\tfstore %d\n", 1.0,gbTmp->index-1); strcat(outputStr[lab], reg);}
	|	IncDecStmt INCREMENT { char reg[200]; if(nowType == 'A') sprintf(reg, "\tldc %d\n\tiadd\n\tistore %d\n", 1,gbTmp->index-1); else sprintf(reg, "\tldc %f\n\tfadd\n\tfstore %d\n", 1.0,gbTmp->index-1); strcat(outputStr[lab], reg);}
	|	IncDecStmt DECREMENT { char reg[200]; if(nowType == 'A') sprintf(reg, "\tldc %d\n\tisub\n\tistore %d\n", 1,gbTmp->index-1); else sprintf(reg, "\tldc %f\n\tfsub\n\tfstore %d\n", 1.0,gbTmp->index-1); strcat(outputStr[lab], reg);}
	|	STORE_ID { $$ = $1 ;}
	|	STORE_INT { $$ = $1; typestack[tyi] = 'I'; tyi++; hi++; nowType = 'I'; char reg[100]; sprintf(reg, "\tldc %d\n", I_data); strcat(outputStr[lab], reg);}
    |	STORE_FLT { $$ = $1; typestack[tyi] = 'F'; tyi++; hf++; nowType = 'F'; char reg[100]; sprintf(reg, "\tldc %f\n", F_data); strcat(outputStr[lab], reg);}
;

hiCALC
	: hiCALC {preType = nowType;} '*' hiCALC { $$ = $1 * $4; checki2f('m');}  
	| hiCALC {preType = nowType;} '/' hiCALC
    {   
		if($4 == 0) 
        { 
            printErrflag = 1; 
            printf(ANSI_COLOR_RED   "<ERROR> The divisor can’t be 0 (line %d)\n"    ANSI_COLOR_RESET, yylineno);
			ifer++;
			hs++;
		}
        else 
        { 
            $$ = $1 / $4;
			checki2f('d');
        }
	}
	| hiCALC {preType = nowType;} '%' hiCALC
	{
        if(preType == 'B' || preType == 'F' || nowType == 'F' || nowType == 'B')
        {
            printf(ANSI_COLOR_RED   "<ERROR> Float type can't Mod (line %d)\n"    ANSI_COLOR_RESET, yylineno);
			ifer++;
			hs++;
        }
        else
        {
			char reg[50];
            $$ = (int)$1 % (int)$4;
			checki2f('M');
        }
	}
	| '(' CALC ')' { $$ = $2; }
	| IncDecStmt
;

CALC
	: hiCALC { $$ = $1; }
	| CALC { preType = nowType;} '+' hiCALC { $$ = $1 + $4; checki2f('a'); }
	| CALC { preType = nowType;} '-' hiCALC { $$ = $1 - $4; checki2f('s'); }
;

RELATIVE
	: CALC { preType = nowType;} '>' CALC { $$ = ($1 > $4);  char reg[100]; sprintf(reg, "\tisub\n\tifle label_%d\n", scopeindex); strcat(outputStr[lab], reg);}
    | CALC { preType = nowType;} '<' CALC { $$ = ($1 < $4);  char reg[100]; sprintf(reg, "\tisub\n\tifge label_%d\n", scopeindex); strcat(outputStr[lab], reg);}
    | CALC { preType = nowType;} EQU CALC { $$ = ($1 == $4); char reg[100]; sprintf(reg, "\tisub\n\tifne label_%d\n", scopeindex); strcat(outputStr[lab], reg);}
    | CALC { preType = nowType;} GRE CALC { $$ = ($1 >= $4); char reg[100]; sprintf(reg, "\tisub\n\tiflt label_%d\n", scopeindex); strcat(outputStr[lab], reg);}
    | CALC { preType = nowType;} LSE CALC { $$ = ($1 <= $4); char reg[100]; sprintf(reg, "\tisub\n\tifgt label_%d\n", scopeindex); strcat(outputStr[lab], reg);}
    | CALC { preType = nowType;} NEQ CALC { $$ = ($1 != $4); char reg[100]; sprintf(reg, "\tisub\n\tifeq label_%d\n", scopeindex); strcat(outputStr[lab], reg);} 
;

lockedID 
	: ID		
	{ 
		char *p = strtok($1, "+-*/()=%><$!@#^& \t"); 
		strcpy(lockedID, p);
		stacksize++;
	}
;

STORE_ID
	: ID		
	{
		char *p = strtok($1, "+-*/()=%><$!@#^& \t");
		strcpy(mID, p); 

		gbTmp = lookup_symbol(mID);
		
		if(!gbTmp)
		{
			printf(ANSI_COLOR_RED   "<ERROR> can't find variable %s (line %d)\n"    ANSI_COLOR_RESET, mID, yylineno);
			ifer++;
		}
		else
		{
			if(gbTmp->mType[0] == 'i')
			{
				$$ = (float)gbTmp->I_data; I_data = (int)$$, F_data = $$;
				nowType = 'A';
				char reg[100];
				sprintf(reg, "\tiload %d\n", gbTmp->index-1);
				strcat(outputStr[lab], reg);
				hi++;

				typestack[tyi] = 'I';
				tyi++;

			}
			else if(gbTmp->mType[0] == 'f')
			{
				isflt = 1;
				$$ = gbTmp->F_data;; I_data = (int)$$, F_data = $$;
				nowType = 'B';
				char reg[100];
                sprintf(reg, "\tfload %d\n", gbTmp->index-1);
				strcat(outputStr[lab], reg);
				hf++;

				typestack[tyi] = 'F';
				tyi++;
			}
		}
		stacksize++;
	}
;

STORE_STR
	: STRING	{ strcpy(mStr, $1); };

STORE_INT
	: I_CONST	{ $$ = (float)$1; I_data = $1; F_data = (float)$1; stacksize++;};

STORE_FLT
	: F_CONST	{ $$ = $1; F_data = $1; I_data = (int)$1; stacksize++;};

trap 	
	: NEWLINE 	{ /* puts("NEWLINE"); */ strcpy(typestack, ""); hs = tyi = na = hf = hi = af = 0; if(stacksize > maxS) maxS = stacksize; stacksize = 0;}
	| Other 	{  /*puts("Other");*/  } 
;

%%

/* C code section */
int main(int argc, char** argv)
{
	yyin = fopen(argv[1], "r");

	yylineno = 0;
	IAlwaysInit();	

	yyparse();
	
	dump_symbol();
	
	return 0;
}

void IAlwaysInit()
{
	Scope = malloc(sizeof(scope));
	Scope -> inScope_list = malloc(sizeof(symbol_table));
	Scope -> inScope_list -> scopeindex = scopeindex;
	Scope -> inScope_head = Scope->inScope_list;
	Scope -> scopeindex = scopeindex;
	MasterScope = Scope;
	MasterScope->mother = malloc(sizeof(scope));

	scopelist[0] = Scope;
	
	int i = 0;
	for(i = 0; i < CSIZE; i++)
	{
		strcpy(outputStr[i], "");
	}

}

void scopefunc(char m)
{
	/*
		每個Scope有自己的Symbol Table
		應該要直接使用scope裡面的inScope_table, inScope_head 才對.
		如果在自己的Symbol Table找不到變數 -> 則尋找Mother.
	*/
	scope *mother;
	scope *child;

	if(m == '{') // 如果遇到左括號
	{
		Scope -> child = malloc(sizeof(scope)); // 分配空間

		mother = Scope; 					// 設給 mother 當前的 Scope

		Scope = Scope -> child; 			// 當前 Scope 設為 child
		Scope -> mother = mother; 			// child 的 mother 等於前面設定好的 mother

		ScopeDepth ++; 						// 深度 + 1

		Scope -> inScope_list = malloc(sizeof(symbol_table));
		Scope -> inScope_head = Scope -> inScope_list;

		scopeindex++; // 每個scope都有自己的 index
		scopelist[scopeindex] = Scope; 
		Scope -> scopeindex = scopeindex;

		l = 1;
	}

	else if(m == '}') // 如果遇到右括號
	{
		child = Scope;
		
		Scope = Scope -> mother; // Scope 設為 mother
		ScopeDepth --; // 深度 - 1

		if(Scope -> mother == NULL)
		{  
			Scope = child;
			yyerror("< } > used without < { >");
			ifer++;
		}
		
		if( l == 1 && r == 0 )
		{
			Scope -> child -> child = malloc(sizeof(scope));
			r = 1;
		}
	}

	if (ScopeDepth == 0)
	{
		r = 0;
	}

	if (MaxScopeDepth < ScopeDepth)
	{
		MaxScopeDepth = ScopeDepth; // 紀錄最大深度
	}
}

void create_symbol() 
{	
	insert_symbol();
}

void insert_symbol() 
{
	declaredtwice = 0;

	if(lookup_symbol(lockedID) && declaredtwice == 0)
	{
		printf(ANSI_COLOR_RED   "<ERROR> re-declaration for variable %s (line %d)\n"    ANSI_COLOR_RESET, lockedID, yylineno);
		ifer++;
		return;
	}

	INDEX++;
	Scope -> inScope_list -> index = INDEX;
	Scope -> inScope_list -> ScopeDepth = ScopeDepth;
	Scope -> inScope_list -> scopeindex = scopeindex;
	strcpy(Scope -> inScope_list -> mID, lockedID);
	strcpy(Scope -> inScope_list -> mType, mType);

	if(na!=0)
		strcat(outputStr[lab], "\tldc 0\n");

	if(hf!=0 && mType[0] == 'i')
	{
		strcat(outputStr[lab], "\tf2i\n");
	} else if ( hf == 0 && mType[0] == 'f') {
		strcat(outputStr[lab], "\ti2f\n");
	}

	char val[20];
	if(mType[0] == 'i')
	{
		Scope -> inScope_list ->I_data = I_data;
		strcpy(val, "\tistore ");
	}
	else if(mType[0] == 'f')
	{
		Scope -> inScope_list ->F_data = F_data;
		strcpy(val, "\tfstore ");
	}	
	
	char reg[20];
	sprintf(reg, "%d\n", INDEX-1);

	strcat(val, reg);
	strcat(outputStr[lab], val);

	Scope -> inScope_list -> next = malloc(sizeof(symbol_table));
	Scope -> inScope_list = Scope-> inScope_list -> next;
}

symbol_table *lookup_symbol(char const *Look_ID) // 只搜尋自己的Scope 找不到再找 mother
{
	scope *tmp = Scope;

	while(tmp -> mother != NULL)
	{
		symbol_table *ttmp  = tmp -> inScope_head;
		
		while(ttmp -> next != NULL)
		{
			if(strcmp(ttmp->mID, Look_ID) == 0)
			{
				return ttmp;
			}
			ttmp = ttmp -> next;
		}

		declaredtwice++;
		tmp = tmp -> mother;
	}

	return NULL;
}

void dump_symbol()
{
	if(ifer!=0)
		return;

	to_j = fopen("test.j", "w+");

	fputs(a, to_j);

    char b[200];

    stacksize = maxS + 10 - (maxS % 10);
	int localsize = INDEX + 10 - (INDEX % 10);

    sprintf(b, ".class public main\n.super java/lang/Object\n.method public static main([Ljava/lang/String;)V\n.limit stack %d\n.limit locals %d\n", stacksize, localsize);

	fputs(b, to_j);

	int i;

	char temp[500];
	
    for(i = 0; i < may; i++)
    {   
		strcpy(temp, outputStr[mayAdd[i]]);
        sprintf(outputStr[mayAdd[i]], "Exit_%d:\n%s", EXITID, temp);
        EXITID++;

    }

	for(i = 0; i <= lab; i++)
	{
/*	
 	//DebugLog
		printf("lab:%d\n", i);
		fputs(outputStr[i], stdout);
*/	
		fputs(outputStr[i], to_j);
	}

	fputs("\treturn\n.end method", to_j);

	fclose(to_j);
}

float Func_Assign(char m, float flt)
{
	gbTmp = lookup_symbol(lockedID);

	af++;

	char reg[200];
	char prefix[100];

	if(!gbTmp)
	{   
		printf(ANSI_COLOR_RED   "<ERROR> can't find variable %s (line %d)\n"    ANSI_COLOR_RESET, lockedID, yylineno);
		ifer++;
	}

	else
	{
		if(gbTmp->mType[0] == 'i')
		{
			if(typestack[tyi-2] == 'F' || typestack[tyi-1] == 'F' && m == '%')
			{
				ifer++;

				if(hs!=0) return 0;

				printf(ANSI_COLOR_RED   "<ERROR> Float type can't Mod (line %d)\n"    ANSI_COLOR_RESET, yylineno);				
				return 0;
			}

			if(flt < 1 && m == '/')
			{
				ifer++;

				if(hs!=0) return 0;

				printf(ANSI_COLOR_RED   "<ERROR> The divisor can’t be 0 (line %d)\n"    ANSI_COLOR_RESET, yylineno);
				return 0;
			}
			
			switch(m)
			{
				case '=' : gbTmp->I_data = (int)flt; break;
				case '+' : gbTmp->I_data += (int)flt; break;
				case '-' : gbTmp->I_data -= (int)flt; break;
				case '*' : gbTmp->I_data *= (int)flt; break;
				case '/' : gbTmp->I_data /= (int)flt; break;
				case '%' : gbTmp->I_data %= (int)flt; break;
			}

			if ( m == '=') sprintf(reg, "\tistore %d\n", gbTmp->index-1);
			else if ( m == '+') sprintf(reg, "\tiload %d\n\tiadd\n\tistore %d\n", gbTmp->index-1, gbTmp->index-1);
			else if ( m == '-') sprintf(reg, "\tistore %d\n\tiload %d\n\tiload %d\n\tisub\n\tistore %d\n", INDEX, gbTmp->index-1, INDEX, gbTmp->index-1);
			else if ( m == '*') sprintf(reg, "\tiload %d\n\timul\n\tistore %d\n", gbTmp->index-1, gbTmp->index-1);
			else if ( m == '/') sprintf(reg, "\tistore %d\n\tiload %d\n\tiload %d\n\tidiv\n\tistore %d\n", INDEX, gbTmp->index-1, INDEX, gbTmp->index-1);
			else if ( m == '%') sprintf(reg, "\tistore %d\n\tiload %d\n\tiload %d\n\tirem\n\tistore %d\n", INDEX, gbTmp->index-1, INDEX, gbTmp->index-1);

			if(hf!=0)
			{
				strcpy(prefix, "\tf2i\n");
				strcat(prefix, reg);
				strcpy(reg, prefix);
			}

			strcat(outputStr[lab], reg);

			return (float)gbTmp->I_data;
		}
		else if(gbTmp->mType[0] == 'f')
		{
			switch(m)
			{
				case '=' : gbTmp->F_data = flt; break;
				case '+' : gbTmp->F_data += flt; break;
				case '-' : gbTmp->F_data -= flt; break;
				case '*' : gbTmp->F_data *= flt; break;
				case '/' : gbTmp->F_data /= flt; break;
				case '%' : printf(ANSI_COLOR_RED   "<ERROR> float type can't MOD (line %d)\n"    ANSI_COLOR_RESET, yylineno); break;
			}

            if ( m == '=') sprintf(reg, "\tfstore %d\n", gbTmp->index-1);
            else if ( m == '+') sprintf(reg, "\tfload %d\n\tfadd\n\tfstore %d\n", gbTmp->index-1, gbTmp->index-1);
            else if ( m == '-') sprintf(reg, "\tfstore %d\n\tfload %d\n\tfload %d\n\tfsub\n\tfstore %d\n", INDEX, gbTmp->index-1, INDEX, gbTmp->index-1);
            else if ( m == '*') sprintf(reg, "\tfload %d\n\tfmul\n\tfstore %d\n", gbTmp->index-1, gbTmp->index-1);
            else if ( m == '/') sprintf(reg, "\tfstore %d\n\tfload %d\n\tfload %d\n\tfdiv\n\tfstore %d\n", INDEX, gbTmp->index-1, INDEX, gbTmp->index-1);

			if(hi!=0 && hf==0)
            {
                strcpy(prefix, "\ti2f\n");
             	strcat(prefix, reg);
				strcpy(reg, prefix);
            }

			strcat(outputStr[lab], reg);
			return gbTmp->F_data;
		}
	}

}

void pfunc(float flt, int mode)
{
	char template_1[110] = "\tgetstatic java/lang/System/out Ljava/io/PrintStream;\n\tswap\n\tinvokevirtual java/io/PrintStream/print(I)V\n";
	char template_2[110] = "\tgetstatic java/lang/System/out Ljava/io/PrintStream;\n\tswap\n\tinvokevirtual java/io/PrintStream/println(I)V\n";

    if (hf==0)
    {   
        if(mode == 1)
			strcpy(template_1, template_2);
    }   
    else
    {   
        if(mode == 0)
		{
			template_1[101] = 'F';
		}
        else if(mode == 1)
		{
			strcpy(template_1, template_2);
			template_1[103] = 'F';
		}
    }

	if(af != 0)
	{
		char reg[20];
		sprintf(reg, "\tiload %d\n", gbTmp->index-1);

		reg[1] = gbTmp->mType[0];

		if(reg[1] == 'i')
			template_1[strlen(template_1)-4] = 'I';
		else
			template_1[strlen(template_1)-4] = 'F';

		strcat(outputStr[lab], reg);
	}

	strcat(outputStr[lab], template_1);
}

void pfuncS(int mode)
{
	char reg[666];
	if(mode == 0)
		sprintf(reg, "\tldc %s\n\tgetstatic java/lang/System/out Ljava/io/PrintStream;\n\tswap\n\tinvokevirtual java/io/PrintStream/print(Ljava/lang/String;)V\n", mStr);

	if(mode == 1)
        sprintf(reg, "\tldc %s\n\tgetstatic java/lang/System/out Ljava/io/PrintStream;\n\tswap\n\tinvokevirtual java/io/PrintStream/println(Ljava/lang/String;)V\n", mStr); 
	strcat(outputStr[lab], reg);
}

void checki2f(char c)
{
	char reg[100];
	/*
	int i = 0;
	for ( i = 0; i < tyi; i++ )
	{
		printf("%c",typestack[i]);
	}
	printf("\n%d\n", tyi);	
	*/
	
	// 如果前後都是整數
	if( typestack[tyi-2] == 'I' && typestack[tyi-1] == 'I' )
	{
		switch(c)
		{
			case 'a' : strcpy(reg, "\tiadd\n"); break;
			case 's' : strcpy(reg, "\tisub\n"); break;
			case 'm' : strcpy(reg, "\timul\n"); break;
			case 'd' : strcpy(reg, "\tidiv\n"); break;
			case 'M' : strcpy(reg, "\tirem\n"); break;
		}
	}
	// 如果前後都是浮點數
	else if ( typestack[tyi-2] == 'F' && typestack[tyi-1] == 'F' )
	{	
		switch(c)
		{
            case 'a' : strcpy(reg, "\tfadd\n"); break;
            case 's' : strcpy(reg, "\tfsub\n"); break;
            case 'm' : strcpy(reg, "\tfmul\n"); break;
            case 'd' : strcpy(reg, "\tfdiv\n"); break;
		}
	}
	// 如果前整後浮
	else if ( typestack[tyi-2] == 'I' && typestack[tyi-1] == 'F' )
	{
		sprintf(reg, "\tfstore %d\n\ti2f\n\tfload %d\n", INDEX, INDEX);

		switch(c)
        {
            case 'a' : strcat(reg, "\tfadd\n"); break;
            case 's' : strcat(reg, "\tfsub\n"); break;
            case 'm' : strcat(reg, "\tfmul\n"); break;
            case 'd' : strcat(reg, "\tfdiv\n"); break;
		}

		typestack[tyi-2] = 'F';
	}   
	// 如果前浮後整
	else
	{
		sprintf(reg, "\ti2f\n");		

        switch(c)
        {
            case 'a' : strcat(reg, "\tfadd\n"); break;
            case 's' : strcat(reg, "\tfsub\n"); break;
            case 'm' : strcat(reg, "\tfmul\n"); break;
            case 'd' : strcat(reg, "\tfdiv\n"); break;
        }
	}

	tyi--;
	strcat(outputStr[lab], reg);
}

void scopetype(char m)
{
	/* F : for 
	 * I : if
	 * J : else if
	 * K : else
	 * B : NORMAL BLOCK;
	 */
	char reg[20] = "";

	if ( m == 'I')
	{
		char label_[20];
		sprintf(label_, "label_%d:\n", lab/2-1);
		strcat(outputStr[lab], label_);
	
		sprintf(reg, "\tgoto Exit_%d\n", EXITID);
		strcat(outputStr[lab-1], reg);

		mayAdd[may] = lab;
		may++;
	}

	else if ( m == 'J')
	{
        char label_[20];
        sprintf(label_, "label_%d:\n", lab/2-1);
        strcat(outputStr[lab], label_);
    
        sprintf(reg, "\tgoto Exit_%d\n", EXITID);
        strcat(outputStr[lab-1], reg);

		mayAdd[may-1] = lab;
	}

	else if ( m == 'K' )
	{
		sprintf(reg, "Exit_%d:\n", EXITID);
		strcat(outputStr[lab-1], reg);
		EXITID++;

		mayAdd[may-1] = 0;
		may--;
	}

	else if ( st == 'B' )
	{
		char label_[20];
		sprintf(label_, "label_%d:\n", lab/2-1);
		strcat(outputStr[lab-1], label_);
	}

	preStack = m;

	st = 'B';
}
